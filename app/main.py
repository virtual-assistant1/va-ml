from flask import Flask, request, jsonify
import joblib as jb
import numpy as np

# weight
# height
# gender
# systolic
# diastolic
# age_year
# cholesterol
# alcohol
# smoke
# glucose
# active
# pulse
rf_joblib = jb.load('./app/CardioPredictorModel.joblib')

app = Flask(__name__)


@app.route("/")
def home_view():
    return "<h1>Welcome to VA ML Team</h1>"


@app.route("/predict", methods=['POST'])
def predict():
    isValid = ValidateRequest()

    if isValid["error"] == True:
        return jsonify(isValid)

    data = request.json
    int_features = [np.array(
        [data["weight"],
         data["height"],
         data["gender"],
         data["systolic"],
         data["diastolic"],
         data["age_year"],
         data["cholesterol"],
         data["alcohol"],
         data["smoke"],
         data["glucose"],
         data["active"],
         data["systolic"] - data["diastolic"]]
    )]
    predictionresult = rf_joblib.predict(int_features)
    if predictionresult == 0:
        return jsonify({"error": False, "message": "You have no Cardiovascular"})
    elif predictionresult == 1:
        return jsonify({"error": False, "message": "You have Cardiovascular"})


def ValidateRequest():
    if request.json == None:
        return {"error": True, "message": "wronge params"}

    data = dict(request.json)

    if "weight" not in data:
        return {"error": True, "message": "weight value is not valid."}

    if "height" not in data:
        return {"error": True, "message": "height value is not valid."}

    if "gender" not in data:
        return {"error": True, "message": "gender value is not valid."}

    if "alcohol" not in data:
        return {"error": True, "message": "alcohol value is not valid."}

    if "smoke" not in data:
        return {"error": True, "message": "smoke value is not valid."}

    if "systolic" not in data:
        return {"error": True, "message": "systolic value is not valid."}

    if "diastolic" not in data:
        return {"error": True, "message": "diastolic value is not valid."}

    if "age_year" not in data:
        return {"error": True, "message": "age_year value is not valid."}

    if "cholesterol" not in data:
        return {"error": True, "message": "cholesterol value is not valid."}

    if "glucose" not in data:
        return {"error": True, "message": "glucose value is not valid."}

    if "active" not in data:
        return {"error": True, "message": "active value is not valid."}

    if not (type(data["systolic"]) == float or type(data["systolic"]) == int):
        return {"error": True, "message": "systolic value is not valid."}

    if not (type(data["diastolic"]) == float or type(data["diastolic"]) == int):
        return {"error": True, "message": "diastolic value is not valid."}

    if type(data["age_year"]) != int:
        return {"error": True, "message": "age_year value is not valid."}

    if type(data["cholesterol"]) != int:
        return {"error": True, "message": "cholesterol value is not valid."}

    if type(data["weight"]) != int:
        return {"error": True, "message": "weight value is not valid."}

    if type(data["height"]) != int:
        return {"error": True, "message": "height value is not valid."}

    if type(data["gender"]) != int:
        return {"error": True, "message": "gender value is not valid."}

    if type(data["alcohol"]) != int:
        return {"error": True, "message": "alcohol value is not valid."}

    if type(data["smoke"]) != int:
        return {"error": True, "message": "smoke value is not valid."}

    if type(data["glucose"]) != int:
        return {"error": True, "message": "glucose value is not valid."}

    if type(data["active"]) != int:
        return {"error": True, "message": "active value is not valid."}

    return {"error": False}
